# p1-tickg

Este proyecto forma parte de la Practica 1 en Iquall Networks.

# Comandos 

## Ejecutar docker-compose

sudo docker-compose up -d

## Detener-Borrar todos los contenedores

sudo docker stop $(sudo docker ps -a -q)

sudo docker rm $(sudo docker ps -a -q)

## Ejecutar ansible local

sudo ansible-playbook --connection=local --inventory localhost, playbook.yml

# Credenciales

bd_influx: influxdb

bd_telegraf: telegraf

user: iquall

pw: 1234
